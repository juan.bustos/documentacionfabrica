[33mcommit e93ed6e592686e48471d6ac559f65820fedaf5e5[m[33m ([m[1;36mHEAD -> [m[1;32mfeature/FCE_17_20190103_CDC[m[33m)[m
Author: Juan Pablo Bustos Sáez <juan.bustos@redsalud.cl>
Date:   Fri Jan 3 13:16:51 2020 -0300

    Fecha :03-01-2020
    Ciclo :1
    Descripción: Modification definición de ambiente
    Responsable: Juan Bustos Sáez.
    QA: Barbara Vega

[33mcommit 3bd7c5456217c09e9509a94afe08691233bd4271[m[33m ([m[1;31morigin/master[m[33m, [m[1;31morigin/HEAD[m[33m, [m[1;32mmaster[m[33m)[m
Merge: 58bd9a7 40ffbf7
Author: Juan Pablo Bustos Saez (RedSalud CMD Casa Matriz) <juan.bustos@redsalud.cl>
Date:   Fri Jan 3 15:40:36 2020 +0000

    Merge branch 'feature/JuanBustosSaez_MySpace' into 'master'
    
    From feature/JuanBustosSaez_MySpace into master
    
    See merge request juan.bustos/documentacionfabrica!2

[33mcommit 40ffbf75574d260b2fb226d61edaeab551c5fd1a[m[33m ([m[1;31morigin/feature/JuanBustosSaez_MySpace[m[33m, [m[1;32mfeature/JuanBustosSaez_MySpace[m[33m)[m
Author: Juan Pablo Bustos Sáez <juan.bustos@redsalud.cl>
Date:   Fri Jan 3 12:39:49 2020 -0300

    Mis Documentaciones

[33mcommit 58bd9a7a04f2bd27ceefcd2c128fa0c943cd7c3d[m
Author: Juan Pablo Bustos Saez (RedSalud CMD Casa Matriz) <juan.bustos@redsalud.cl>
Date:   Fri Jan 3 15:11:35 2020 +0000

    Initial commit
